from flask import Flask, render_template, request, redirect, Response , jsonify
from pymongo import MongoClient
import random, json
from flask_cors import CORS
app = Flask(__name__)
CORS(app)
client = MongoClient('localhost', 27017)
#db = client.admin (for argos server)
db = client.admin #call db
#collections = db.anomaly (for argos server)
collections = db.anomaly #call collection

@app.route('/', methods = ['GET'])
def output():

	return "testtest"

@app.route('/receiver', methods = ['POST'])
def SubmitFrom():
	portjson = request.json # read json + reply
	print(portjson)
	print("aaaaaaaaaaaaaaaaaa")
	collections.insert(portjson)
	data_cursor = collections.find({})
	print (data_cursor.count())
	return "result"

@app.route('/receiver/post', methods = ['POST'])
def get_all_frameworks():
    data = request.json['metric_addr']
    print(data)
    dataquery = []
    for data_cursor in collections.find({'metric_addr' : data}):
        print(data_cursor)
        dataquery.append({'start' : data_cursor['start'],
                    'end' : data_cursor['end'],
                    'metric_addr' : data_cursor['metric_addr'],
                    'model_name' : data_cursor['model_name'],
                    'model_param' : data_cursor['model_param'],
                    'allAnomally' : data_cursor['allAnomally'],
                    'correctAnomally' : data_cursor['correctAnomally']})
    return jsonify({"alldata" : dataquery})

if __name__ == '__main__':
	# run!
	app.debug = True
	app.run(host='0.0.0.0',port=5001)
