from flask import Flask, render_template, request, redirect, Response , jsonify
from pymongo import MongoClient
from bson.objectid import ObjectId
import random, json
from flask_cors import CORS
app = Flask(__name__)

class ArUiConst:
	##### REST INTERFACE #####
	rstAllowedAddr   = '0.0.0.0'
	rstListeningPort = 5001

	##### MongoDB  #####
	mgdbServer     = 'localhost'
	mgdbServerPort = 27017
	mgdbDatabase   = 'admin'
	mgdbCollection = 'anomaly'

@app.route('/receiver', methods = ['POST'])
def SubmitFrom():
	# print("hello")
	portjson = request.json # read json + reply
	print("aaaaaaaaaaaaaaaaaa")
	data_cursor = db.anomaly.find()

	print("28")
	print(portjson)
	metricq = request.json['metric_addr']
	startq = request.json['start']
	endq = request.json['end']
	modelq = request.json['model_name']
	paramq = request.json['model_param']
	print(metricq)
	print(startq)
	print(endq)
	print(modelq)
	print(paramq)
	metric_new = request.json['metric_addr']
	start_new = request.json['start']
	end_new = request.json['end']
	model_new = request.json['model_name']
	param_new = request.json['model_param']
	allanomally_new = request.json['allAnomally']
	correctanomally_new = request.json['correctAnomally']
	cur = db.anomaly.find({'metric_addr' : metricq , 'start' : startq ,'end' : endq  ,'model_name' : modelq ,'model_param' : paramq})
	print("54")
	query="";
	print("38")
	if cur.count() == 0:
		print("insert")
		db.anomaly.insert(portjson)
		# print("insert")
	else:
		print(cur)
		for data in cur :
			print("update")
			test = data['_id']
			print(test)
			break
		db.anomaly.update({u'_id':ObjectId(test)}, {"$set": { 'metric_addr': metric_new , 'start': start_new ,'end': end_new , 'model_name': model_new ,'model_param': param_new ,'allAnomally': allanomally_new , 'correctAnomally': correctanomally_new}})

	return "result"

@app.route('/receiver/post', methods = ['POST'])
def get_all_frameworks():
    data = request.json['metric_addr']
    print(data)
    dataquery = []
    for data_cursor in collections.find({'metric_addr' : data}):
        print(data_cursor)
        dataquery.append({'start' : data_cursor['start'],
                    'end' : data_cursor['end'],
                    'metric_addr' : data_cursor['metric_addr'],
                    'model_name' : data_cursor['model_name'],
                    'model_param' : data_cursor['model_param'],
                    'allAnomally' : data_cursor['allAnomally'],
                    'correctAnomally' : data_cursor['correctAnomally']})
    return jsonify({"alldata" : dataquery})

if __name__ == '__main__':

	CORS(app)
	client = MongoClient(ArUiConst.mgdbServer, ArUiConst.mgdbServerPort)
	db = client[ArUiConst.mgdbDatabase]
	collections = db[ArUiConst.mgdbCollection]

	app.debug = True
	app.run(host=ArUiConst.rstAllowedAddr, port=ArUiConst.rstListeningPort)
