var myConfig = {
  "type":"line",
  "series":[
    {
      "values":[3,4,11,5,19,7],
      "line-color":"red",
      "line-style":"dashed",
      "marker":{
        "background-color":"orange",
        "size":9
      }
    },
    {
      "values":[9,19,15,25,12,14],
      "line-color":"gray",
      "line-width":1,
      "line-style":"solid",
      "marker":{
        "background-color":"green",
        "size":7,
        "border-color":"pink",
        "border-width":2
      }
    },
    {
      "values":[15,29,19,21,25,26],
      "line-color":"blue",
      "line-style":"dotted",
      "marker":{
        "background-color":"purple",
        "size":5
      }
    }
  ]
};

zingchart.render({
	id : 'myChart',
	data : myConfig,
	height: "100%",
	width: "100%"
});
